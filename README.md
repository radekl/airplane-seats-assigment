# Assign seat on airplane


Very simple script that assignes for passenger to airplane.

The airplane is configured by basci YAML config file with this
required fields:
 - rows
 - seats

Where rows are max number of airplane rows and seats
als alpphabet letters.
Example for [Boeign 737-800 config](config.yml)
is based on [Norwegians seats map]
(https://www.norwegian.com/us/travel-info/on-board/seat-reservation/seat-maps/).

To change the airplane config modify the config.yaml file.

The script creates seats_assigment.csv file with format:
```
ID, seat
```

## Running script

The script requires Python3.7 and higer

 1. clone the repository and navigate to the directory.
 2. install Python dependancy package:
    ```
    python -m pip install -r requirements.txt
    ```
 3. navigate to src directory and run the script

Examples:
 - Show script help
```
python assign.py --help
```

 - Assign random available seat to passenger with id 123
```
python assign.py 123
```
- Assign seat 12A to passenger with id 1234
```
python assign.py 123 --seat 12A
```

## TODO
- provide mulitple passegners assigment (make cmd required id to accept multiple values)
- user ccan change name of output csv file (add and handle command line argument)
- create multiple function strategies to assign seat (now usere can either specify seat or seat is assigned randomly
but it coudl be possible to assigned seat together for couples f.e.)
- make Business class seats (add another field in YAML config file which specifyes bussiness class rows and modify the code)
- setup CI to run tests