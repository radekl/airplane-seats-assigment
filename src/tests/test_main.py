import pytest
import os
import random, string

from src.assign import main, OUTPUT_CSV_FILE, read_output


def random_id():
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(5))


def test_assign_seat():
    if os.path.exists(OUTPUT_CSV_FILE):
        os.remove(OUTPUT_CSV_FILE)
    id = random_id()
    seat = main(id=id)
    assert id in read_output().keys()
    assert seat in read_output().values()
    os.remove(OUTPUT_CSV_FILE)


def test_exception_assign_existing_id():
    if os.path.exists(OUTPUT_CSV_FILE):
        os.remove(OUTPUT_CSV_FILE)
    id = random_id()
    main(id=id)
    with pytest.raises(ValueError):
        main(id=id)
    os.remove(OUTPUT_CSV_FILE)

