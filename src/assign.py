#!/usr/bin/env python3

"""
Script to assign seats to passenger.

Input is yaml configuration file which requires three keywords:
- type
- rows
- seats

Example config.yml file is based on:
Airplane: Boeign 737-800
(https://www.norwegian.com/us/travel-info/on-board/seat-reservation/seat-maps/)


"""

from pathlib import Path
import random
from typing import Dict, List
import argparse
import csv
import yaml


CONFIG_FILE = Path.cwd() / '..' / 'config.yml'
OUTPUT_CSV_FILE = "seats_assigment.csv"


def config() -> Dict:
    """
    Airplane configuration file.

    Returns
    -------
    dict
        airplane configuration file
    """
    with open(CONFIG_FILE, 'r') as file:
        input_config = yaml.safe_load(file)

    # validate all required keywords
    if not all(keyword in input_config.keys()
               for keyword in ['rows', 'seats']):
        raise ValueError(
            f'File {CONFIG_FILE} does not contain all required keywords.')
    return input_config


def read_output() -> Dict:
    """
    Reading output csv file.

    Returns
    -------
    dict:
        passenger as key, with seat assigned as value

    """
    ret_dict = {}
    try:
        with open(OUTPUT_CSV_FILE, 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                ret_dict[row[0]] = row[1]
    except FileNotFoundError:
        pass

    return ret_dict


def write_to_output(id: 'str', seat: str):
    """
    Writer passenger id and assigned seat to oputpu file.

    Parameters
    ----------
    id : str
        passenger ID
    seat : str
        assigned seat
    """
    with open(OUTPUT_CSV_FILE, 'a+', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([id, seat])


def id_exists(id: str) -> bool:
    """
    Check if passanger with given ID has seat already assigned.

    Parameters
    ----------
    id : str
        passenger ID

    Returns:
    bool
        True if ID already has seat assigned
    """
    return id in read_output().keys()


def seats() -> List[str]:
    """
    All existing seats in airplane.

    Returns
    -------
    list
        all possible seats
    """
    seats = []
    for row in range(1, config()['rows'] + 1):
        for seat in config()['seats']:
            seats.append(f"{row}{seat}")
    return seats


def available_seats() -> List[str]:
    """
    All available seats left in airplane.

    Returns
    -------
    list
        list of available seats
    """
    return list(set(seats()) - set(read_output().values()))


def assign_random():
    """
    Select random seat.

    Returns
    -------
    str
        assigned seat
    """
    return random.choice(available_seats())


def main(id: str, selected_seat: str = None):
    """
    Assign seat to passenger ID. Seat can be manually selected.

    Parameters
    ----------
    id : str
        Passenger id
    selected_seat : str
        manually selected seat

    Returns
    -------
    seat : str
        seat number

    Raises
    ------
    RuntimeError
        When 


    """
    if len(available_seats()) == 0:
        raise RuntimeError("The airplane is full!")
    if id_exists(id):
        raise ValueError(f"ID: {id} already have seat.")
    if selected_seat and selected_seat not in seats():
        raise ValueError(
            f"Seat {selected_seat} does not exists on this airplane!")
    if selected_seat and selected_seat not in available_seats():
        raise ValueError(
            f"Sorry but seat {selected_seat} is already taken.")

    assigned_seat = selected_seat if selected_seat else assign_random()
    write_to_output(id, assigned_seat)
    return assigned_seat


if __name__ == "__main__":
    """
    Main function to assigne passenger seat.
    """
    parser = argparse.ArgumentParser(
        description="""Assigne passanger with ID seat in the airplane
                Random seat will be assigned if seat not specified.
        """
    )
    parser.add_argument('id',
                        type=str,
                        help='Passenger id (as str)'
                        )
    parser.add_argument('--seat',
                        help='Selected seat.'
                        )
    args = parser.parse_args()

    id = args.id
    selected_seat = args.seat.upper() if args.seat else None

    try:
        result = main(id=id, selected_seat=selected_seat)
    except Exception as e:
        exit(f"Can't assign seat, reason: {e}")

    exit(f"Assigned seat {result}")
